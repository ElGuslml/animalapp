import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';

import 'package:animalapp/src/pages/home_page.dart';
import 'package:animalapp/src/routes/routes.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final pages = [
    PageViewModel(
        pageColor: const Color(0xFF03A9F4),
        // iconImageAssetPath: 'assets/air-hostess.png',
        bubble: Image.asset('assets/pict1.png'),
        body: Text(
          'Las probabilidades de que una especie siga existiendo en el corto o mediano plazo',
        ),
        title: Text(
          'Amenazas',
        ),
        titleTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
        bodyTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
        mainImage: Image.asset(
          'assets/pict2.png',
          height: 285.0,
          width: 285.0,
          alignment: Alignment.center,
        )),
    PageViewModel(
      pageColor: const Color(0xFF8BC34A),
      iconImageAssetPath: 'assets/waiter.png',
      body: Text(
        'México es el segundo lugar con el mayor número de especies en peligro crítico de extinción',
      ),
      title: Text('Sabias que'),
      mainImage: Image.asset(
        'assets/pict3.png',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      titleTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
      bodyTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
    ),
    PageViewModel(
      pageColor: const Color(0xFF607D8B),
      iconImageAssetPath: 'assets/taxi-driver.png',
      body: Text(
        'Ayuda a no destruir los ecosistemas de las especies',
      ),
      title: Text('Por Favor'),
      mainImage: Image.asset(
        'assets/pict4.png',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      titleTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
      bodyTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'AnimalApp',
      theme: ThemeData(
          primaryColorDark: Color(0xFF004C40),
          primaryColor: Color(0xFF00796B),
          primaryColorLight: Color(0xFF48A999),
          accentColor: Color(0xFF263238)),
      home: Builder(
        builder: (context) => IntroViewsFlutter(
          pages,
          showNextButton: true,
          showBackButton: false,
          onTapDoneButton: () {
            Navigator.pushNamed(context, 'home');
          },
        ),
      ),
      //initialRoute: '/',
      routes: getApplicationRoutes(),
      onGenerateRoute: (settings) {
        return MaterialPageRoute(builder: (BuildContext context) => HomePage());
      },
    );
  }
}
