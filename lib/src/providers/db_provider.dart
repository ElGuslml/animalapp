import 'dart:io';

import 'package:animalapp/src/models/animal_model.dart';
import 'package:animalapp/src/models/cromos_model.dart';
import 'package:animalapp/src/models/cuestionario_model.dart';
import 'package:animalapp/src/models/siluetas_model.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DBProvider {

  static Database _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database> get database async {

    if(_database != null) return _database;

    _database = await initDB();
    return _database;

  }

  initDB() async {

    Directory documentsDirectory = await getApplicationDocumentsDirectory();

    final path = join(documentsDirectory.path, 'animApp.db');

    // Should happen only the first time you launch your application
    print("Creating new copy from asset");

    // Make sure the parent directory exists
    try {
      await Directory(dirname(path)).create(recursive: true);
    } catch (_) {}
      
    // Copy from asset
    ByteData data = await rootBundle.load(join("assets", "animalapp.db"));
    List<int> bytes =
    data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    
    // Write and flush the bytes written
    await File(path).writeAsBytes(bytes, flush: true);

    return await openDatabase(
      path,
      version: 4,
      onOpen: (db) {},
    );

  }

  Future<List<AnimalModel>> getAnimales() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM animales");
    List<AnimalModel> list = res.isNotEmpty ? res.map((c) => AnimalModel.fromJson(c)).toList() : [];
    return list;
  }

  Future<List<SiluetasModel>> getSiluets() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM siluetas");
    List<SiluetasModel> list = res.isNotEmpty ? res.map((c) => SiluetasModel.fromJson(c)).toList() : [];
    return list;
  }

  Future<List<CromosModel>> getCromos() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM cromos");
    List<CromosModel> list = res.isNotEmpty ? res.map((e) => CromosModel.fromJson(e)).toList() : [];
    return list;
  }

  Future<int> updateCromo(int id) async {
    final db = await database;
    final res = await db.rawUpdate("UPDATE cromos SET status = 'TRUE' WHERE id_cromo = $id");
    return res;
  }

  getAnimal(int id) async {

  }

  Future<List<CuestionarioModel>> getCantCuest() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM cuestionario");
    List<CuestionarioModel> list = res.isNotEmpty ? res.map((e) => CuestionarioModel.fromJson(e)).toList() : [];
    return list;
  }

}