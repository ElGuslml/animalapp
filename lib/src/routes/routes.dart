import 'package:animalapp/src/pages/album_page.dart';
import 'package:animalapp/src/pages/animal_detalles_page.dart';
import 'package:animalapp/src/pages/cromos/ganotrescromo_page.dart';
import 'package:animalapp/src/pages/cromos/perdiocromo_page.dart';
import 'package:animalapp/src/pages/cuestionario_page.dart';
import 'package:animalapp/src/pages/games_page.dart';
import 'package:animalapp/src/pages/memorama_page.dart';
import 'package:flutter/material.dart';

import 'package:animalapp/src/pages/estampa_page.dart';
import 'package:animalapp/src/pages/home_page.dart';
import 'package:animalapp/src/pages/animales_page.dart';
import 'package:animalapp/src/pages/sombras_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    'home': (BuildContext context) => HomePage(),
    'animales': (BuildContext context) => AnimalesPage(),
    'animalDetalles': (BuildContext context) => AnimalDetallesPage(),
    'album': (BuildContext context) => AlbumPage(),
    'juegos': (BuildContext context) => GamesPage(),
    'cuestionario': (BuildContext context) => CuestionarioPage(),
    'memorama': (BuildContext context) => MemoramaPage(),
    'sombras': (BuildContext context) => SombrasPage(),
    'estampa': (BuildContext context) => EstampaPage(),
    'perdioCromo': (BuildContext context) => PerdioCromoPage(),
    'ganoCromos': (BuildContext context) => GanoTresCromoPage()
  };
}
