import 'package:flutter/cupertino.dart';

import 'card_image.dart';

class CardImageList extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        height: 350.0,
        child: ListView(
          padding: EdgeInsets.all(25.0),
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            CardImge("assets/img/pic1.jpg"),
            CardImge("assets/img/pic2.jpg"),
            CardImge("assets/img/pic3.jpg"),
            CardImge("assets/img/pic4.jpg"),
            CardImge("assets/img/pic5.jpg"),
            CardImge("assets/img/pic6.jpg"),
            CardImge("assets/img/pic7.jpg"),

          ],
        )
    );
  }

}