import 'package:flutter/material.dart';

class DescripionPlace extends StatelessWidget{

  String namePlace;
  String descriptionPlace;

  DescripionPlace(this.namePlace, this.descriptionPlace);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final description = Container(
      margin: EdgeInsets.only(
        top: 20.0,
        left: 20.0,
        right: 20.0,
      ),
      child: new Text(
        descriptionPlace,
      ),
    );

    final title_stars = Row (
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
              top: 20.0,
              left: 20.0,
              right: 20.0
          ),

          child: Text(
            namePlace,
            style: TextStyle(
                fontFamily: "Spartan",
                fontSize: 30.0,
                fontWeight: FontWeight.w900
            ),
            textAlign: TextAlign.left,
          ),
        ),


      ],
    );

    return Column(
      children: <Widget>[
        title_stars,
        description,
      ],
    );
  }

}