import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import './Home/description_place.dart';
import './Home/review_list.dart';
import './Home/header_appbar.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  String descripcion = "Existen múltiples causas por las que una especie puede llegar a encontrarse al borde de la extinción. Las razones pueden resultar tremendamente particulares para cada especie, pero en líneas generales, entre las mayores amenazas se encuentra la destrucción y fragmentanción de sus hábitats; el cambio climático; la caza y tráfico ilegal; y la introducción de especies exóticas. ";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Bienvenido"),
        elevation: 0,
      ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.home), title: Text('Home')),
            BottomNavigationBarItem(
              icon: Icon(Icons.pets),
              title: Text('Animales'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.photo_album),
              title: Text('Album'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.games),
              title: Text('Juegos'),
            )
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Theme.of(context).primaryColor,
          onTap: (index) {
            setState(() {
              _selectedIndex = index;
              switch (index) {
                case 0:
                  break;
                case 1:
                  Navigator.pushReplacementNamed(context, 'animales');
                  break;
                case 2:
                  Navigator.pushReplacementNamed(context, 'album');
                  break;
                case 3:
                  Navigator.pushReplacementNamed(context, 'juegos');
                  break;
                default:
              }
            });
          },
        ),
        body: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                HeaderAppBar(),
                DescripionPlace("Sabias que...", descripcion),
                ReviewList(),
                SizedBox(height: 20,)
              ],
            ),
            
          ],
        ),
    );
  }
}
