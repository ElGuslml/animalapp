import 'package:animalapp/src/models/cromos_model.dart';
import 'package:animalapp/src/providers/db_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class AlbumPage extends StatefulWidget {
  AlbumPage({Key key}) : super(key: key);

  @override
  _AlbumPageState createState() => _AlbumPageState();
}

class _AlbumPageState extends State<AlbumPage> {
  int _selectedIndex = 2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.home), title: Text('Home')),
            BottomNavigationBarItem(
              icon: Icon(Icons.pets),
              title: Text('Animales'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.photo_album),
              title: Text('Album'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.games),
              title: Text('Juegos'),
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Theme.of(context).primaryColor,
          onTap: (index) {
            setState(() {
              _selectedIndex = index;
              switch (index) {
                case 0:
                  Navigator.pushReplacementNamed(context, 'home');
                  break;
                case 1:
                  Navigator.pushReplacementNamed(context, 'animales');
                  break;
                case 2:
                  break;
                case 3:
                  Navigator.pushReplacementNamed(context, 'juegos');
                  break;
                default:
              }
            });
          },
        ),
        
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ftrBuild(),
        ));
  }

  Widget ftrBuild() {
    return FutureBuilder(
      future: DBProvider.db.getCromos(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return _crearGridView(snapshot.data);
        }else{
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _crearGridView(List<CromosModel> cromos) {
    return StaggeredGridView.countBuilder(
      crossAxisCount: 4,
      itemCount: cromos.length,
      // itemBuilder: (BuildContext context, int index) => new Container(
      //     color: Colors.green.withOpacity(0.7),
      // ),
      itemBuilder: (context, index) {
        if (cromos[index].status == "FALSE") {
          return new Container(
            color: Colors.green.withOpacity(0.7),
            child: Center(
              child: CircleAvatar(
                backgroundColor: Colors.white,
                child: Text('?'),
              )
            ),
          );
        }else{
          return new Image.network(
            cromos[index].rutaImg,
            fit: BoxFit.cover,
          );
        }
      },
      staggeredTileBuilder: (int index) =>
          new StaggeredTile.count(2, index.isEven ? 2 : 1),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    );
  }
}
