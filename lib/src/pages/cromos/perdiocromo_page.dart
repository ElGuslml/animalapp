import 'package:flutter/material.dart';

class PerdioCromoPage extends StatefulWidget {
  PerdioCromoPage({Key key}) : super(key: key);

  @override
  _PerdioCromoPageState createState() => _PerdioCromoPageState();
}

class _PerdioCromoPageState extends State<PerdioCromoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('SUERTE PARA LA PROXIMA'),
        centerTitle: true,
        elevation: 0,
      ),
      body: ListView(
        children: <Widget>[
          Text('¡Sigue intentandolo! No te des por vencido', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
          SizedBox(height: 20),
          Container(
            child: new LayoutBuilder(builder: (context, constraint) {
              return new Icon(Icons.face, size: 300, color: Colors.white.withOpacity(0.9),);
            })
          ),
          SizedBox(height: 20),
          Padding(
            padding: EdgeInsets.all(8),
            child: SizedBox(
              width: double.infinity,
              child: RaisedButton(
                child: Text('Regresar a Juegos'),
                onPressed: () {
                  Navigator.pushReplacementNamed(context, 'juegos');
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}