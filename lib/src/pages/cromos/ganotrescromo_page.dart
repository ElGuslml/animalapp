import 'package:animalapp/src/models/cromos_model.dart';
import 'package:animalapp/src/providers/db_provider.dart';
import 'package:flutter/material.dart';

class GanoTresCromoPage extends StatefulWidget {
  GanoTresCromoPage({Key key}) : super(key: key);

  @override
  _GanoTresCromoPageState createState() => _GanoTresCromoPageState();
}

class _GanoTresCromoPageState extends State<GanoTresCromoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("¡Felicidades!"),
        centerTitle: true,
        elevation: 0,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            SizedBox(height: 10,),
            Text('Haz ganado estas estampa', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
            SizedBox(height: 20,),
            ftrBuild(),
            crearBtn(),
          ],
        ),
      ),
    );
  }

  Widget crearBtn() {
    return SizedBox(
      width: double.infinity,
      child: RaisedButton(
        child: Text('Agregar al album'),
        onPressed: () {
          Navigator.pushReplacementNamed(context, 'album');
        },
      ),
    );
  }

  Widget ftrBuild() {
    return FutureBuilder(
      future: DBProvider.db.getCromos(),
      builder: (BuildContext context, AsyncSnapshot<List<CromosModel>> snapshot) {
        if (snapshot.hasData) {
          List<CromosModel> cromos = snapshot.data;
          cromos.shuffle();

          updateTabla(cromos[0].idCromo);
          updateTabla(cromos[1].idCromo);
          updateTabla(cromos[2].idCromo);

          return GridView.builder(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1),
            itemBuilder: (context, index) => Image.network(
              cromos[index].rutaImg,
              fit: BoxFit.cover,
            ),
            itemCount: 3,
          );

        }else{
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  updateTabla(int id) async {
    int res = await DBProvider.db.updateCromo(id);
  }

}