import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../pages/Home/card_image.dart';

class GamesPage extends StatefulWidget {
  GamesPage({Key key}) : super(key: key);

  @override
  _GamesPageState createState() => _GamesPageState();
}

class _GamesPageState extends State<GamesPage> {
  int _selectedIndex = 3;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Mini-Juegos'),
        centerTitle: true,
        elevation: 0.0,
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.home), title: Text('Home')),
          BottomNavigationBarItem(
            icon: Icon(Icons.pets),
            title: Text('Animales'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.photo_album),
            title: Text('Album'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.games),
            title: Text('Juegos'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Theme.of(context).primaryColor,
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
            switch (index) {
              case 0:
                Navigator.pushReplacementNamed(context, 'home');
                break;
              case 1:
                Navigator.pushReplacementNamed(context, 'animales');
                break;
              case 2:
                Navigator.pushReplacementNamed(context, 'album');
                break;
              case 3:
                break;
              default:
            }
          });
        },
      ),
      body: ListView(
        children: <Widget>[
          SizedBox(height: 20),
          Text('¡Gana estampas para tu álbum con estos divertidos minijuegos!', style: TextStyle(color: Colors.white), textAlign: TextAlign.center,),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: InkWell(child: juegoSombras(), onTap: (){Navigator.pushNamed(context, 'sombras');},),
          ),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: InkWell(child: memorama(), onTap: () {Navigator.pushNamed(context, 'memorama');},),
          ),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: InkWell(child: quiz(), onTap: () {Navigator.pushNamed(context, 'cuestionario');},),
          )
        ],
      )
    );
  }

  Widget juegoSombras() {
    return Container(
      height: 300,
      width: 100,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage('assets/Juego_Sombras.JPG')
        )
      ),
    );
  }

  Widget memorama() {
    return Container(
      height: 300,
      width: 100,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage("assets/memorama.jpg")
        )
      ),
    );
  }

  Widget quiz() {
    return Container(
      height: 300,
      width: 100,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage("assets/cuestionario.jpg")
        )
      ),
    );
  }

  
}