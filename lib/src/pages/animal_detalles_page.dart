import 'package:animalapp/src/models/animal_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:just_audio/just_audio.dart';
import 'package:latlong/latlong.dart';

class AnimalDetallesPage extends StatefulWidget {
  AnimalDetallesPage({Key key}) : super(key: key);

  @override
  _AnimalDetallesPageState createState() => _AnimalDetallesPageState();
}

class _AnimalDetallesPageState extends State<AnimalDetallesPage> {
  int _selectedIndex = 1;

  @override
  Widget build(BuildContext context) {

    AnimalModel animal = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      body: CustomScrollView(
        shrinkWrap: true,
        slivers: <Widget>[
          _crearAppBar(animal.rutaImg, animal.nombre, animal.status, animal.rutaSonido),
          SliverList(
            delegate: SliverChildListDelegate([
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 32, vertical: 24),
                child: Text(
                   animal.descripcion,
                  textAlign: TextAlign.justify,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 32, vertical: 0),
                child: Text(
                  'Habitat',
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 32, vertical: 16),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20.0),
                        child: Image(
                          image: NetworkImage(
                              animal.rutaHab),
                          fit: BoxFit.cover,
                          height: 200,
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.symmetric(horizontal: 20)),
                    Expanded(
                      child: Text(
                          animal.habitat),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 32, vertical: 0),
                child: Text(
                  'Lugares del mundo',
                  style: Theme.of(context).textTheme.headline5,
                  textAlign: TextAlign.right,
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 32, vertical: 24),
                child: Text(
                     animal.lugar,
                    textAlign: TextAlign.justify,
                ),
              ),
              _crearFlutterMap(animal.coordenadas),

            ]),
          ),
        ],
      ),
    );
  }

  Widget _crearFlutterMap(String Cordenadas) {


      final val = Cordenadas.split(',');
      final lat1 = double.parse(val[0]);
      final lat2 = double.parse(val[1]);

      print(lat1);
      print(lat2);

    return Container(
      height: 300,
      child: FlutterMap(
        options:
            MapOptions(center: LatLng(lat1, lat2), zoom: 5),
        layers: [_crearMapa()],
      ),
    );
  }

  _crearMapa() {
    return TileLayerOptions(
        urlTemplate:
            'https://api.mapbox.com/v4/{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}',
        additionalOptions: {
          'accessToken':
              'pk.eyJ1IjoiZWxndXNsbWwiLCJhIjoiY2s5bjhlZmJoMGV4aTNsdHN6bDNqZGlrayJ9.x2Ns2wZSrGSGRLI7ciYlSg',
          'id': 'mapbox.satellite',
        });
  }

  Widget _crearAppBar( String image, String nombre, String status, String audio ) {
    
    print(audio);
    
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Theme.of(context).accentColor,
      expandedHeight: 250,
      floating: false,
      pinned: false,
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.volume_up), 
          onPressed: () async {
            final player = AudioPlayer();
            var duration = await player.setUrl(audio);
            player.play();
          }
        )
      ],
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(nombre+"\n"+status, textAlign: TextAlign.center,),
        background: Image(
          image: NetworkImage(
              image),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
