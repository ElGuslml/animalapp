import 'package:animalapp/src/models/animal_model.dart';
import 'package:animalapp/src/providers/db_provider.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';

class MemoramaPage extends StatefulWidget {
  MemoramaPage({Key key}) : super(key: key);

  @override
  _MemoramaPageState createState() => _MemoramaPageState();
}


class _MemoramaPageState extends State<MemoramaPage> {

  List<GlobalKey<FlipCardState>> cardStateKeys = [
    GlobalKey<FlipCardState>(),
    GlobalKey<FlipCardState>(),
    GlobalKey<FlipCardState>(),
    GlobalKey<FlipCardState>(),
    GlobalKey<FlipCardState>(),
    GlobalKey<FlipCardState>(),
    GlobalKey<FlipCardState>(),
    GlobalKey<FlipCardState>(),
    GlobalKey<FlipCardState>(),
    GlobalKey<FlipCardState>(),
  ];
  List<String> lista = new List();
  bool primero = false;
  List<bool> cardFlips = [true, true, true, true, true, true, true, true, true, true];
  int prevIndex = -1;
  bool flip = false;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text('Memorama'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: <Widget>[
            Text('¡Responde qué animal es correctamente para ganar una estampa para tu álbum!', textAlign: TextAlign.center,),
            SizedBox(height: 20,),
            ftrImg(),
          ],
        ),
      ),
    );
  }

  Widget ftrImg() {
    return FutureBuilder(
      future: DBProvider.db.getAnimales(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return tarjetas(snapshot.data);
        }else{
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget tarjetas(List<AnimalModel> animales) {

    animales.shuffle();

    if (!primero) {  
      int c = 0;
      for (var i = 0; i < animales.length; i++) {
        if(c < 5)
        {
          lista.add(animales[i].rutaImg);
          lista.add(animales[i].rutaImg);
          c++;
        }
      }
      lista.shuffle();
      primero = true;
    }

    return GridView.builder(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2
      ),
      itemBuilder: (context, index) => FlipCard(
        key: cardStateKeys[index],
        onFlip: () {
          if (!flip) {
            flip = true;
            prevIndex = index;
          }else{
            flip = false;
            if (prevIndex != index) {
              if (lista[prevIndex] != lista[index]) {
                cardStateKeys[prevIndex].currentState.toggleCard();
                prevIndex = index;
              }else{
                cardFlips[index] = false;
                cardFlips[prevIndex] = false;
                setState(() {
                  
                });
                if (cardFlips.every((t) => t == false)) {
                  Navigator.pushNamed(context, 'estampa');
                }
              }
            }
          }
        },
        direction: FlipDirection.HORIZONTAL,
        flipOnTouch: cardFlips[index],
        front: Padding(
          padding: const EdgeInsets.all(5),
          child: Container(
            color: Colors.green.withOpacity(0.3)
          ),
        ),
        back: Padding(
          padding: EdgeInsets.all(5),
          child: Image.network(lista[index], fit: BoxFit.cover, )
        )
      ),
      itemCount: lista.length,
    );

  }

}