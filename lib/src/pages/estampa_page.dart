import 'package:animalapp/src/models/cromos_model.dart';
import 'package:animalapp/src/providers/db_provider.dart';
import 'package:flutter/material.dart';

class EstampaPage extends StatefulWidget {
  EstampaPage({Key key}) : super(key: key);

  @override
  _EstampaPageState createState() => _EstampaPageState();
}

class _EstampaPageState extends State<EstampaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("¡Felicidades!"),
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: ListView(
        children: <Widget>[
          SizedBox(height: 10,),
          Text('Haz ganado esta estampa', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
          SizedBox(height: 20),
          Padding(
            padding: EdgeInsets.all(20),
            child: cromo(),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: SizedBox(
              width: double.infinity,
              child: RaisedButton(
                child: Text('Agregar al Album'),
                onPressed: () {
                  Navigator.pushReplacementNamed(context, 'album');
                }
              )
            ),
          )
        ],
      )
    );
  }

  Widget cromo() {
    return FutureBuilder(
      future: DBProvider.db.getCromos(),
      builder: (BuildContext context, AsyncSnapshot<List<CromosModel>> snapshot) {
        if (snapshot.hasData) {
          
          List<CromosModel> cromos = snapshot.data;
          cromos.shuffle();

          updateTabla(cromos[0].idCromo);

          return Image.network(cromos[0].rutaImg, fit: BoxFit.cover,);

        }else{
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  updateTabla(int id) async {
    int res = await DBProvider.db.updateCromo(id);
    print(res);
  }

}