import 'package:animalapp/src/models/animal_model.dart';
import 'package:animalapp/src/providers/db_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class AnimalesPage extends StatefulWidget {
  AnimalesPage({Key key}) : super(key: key);

  @override
  _AnimalesPageState createState() => _AnimalesPageState();
}

class _AnimalesPageState extends State<AnimalesPage> {
  int _selectedIndex = 1;

  @override
  Widget build(BuildContext context) {
    obtenerAnimales();
    return SafeArea(
      top: false,
      child: Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.home), title: Text('Home')),
            BottomNavigationBarItem(
              icon: Icon(Icons.pets),
              title: Text('Animales'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.photo_album),
              title: Text('Album'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.games),
              title: Text('Juegos'),
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Theme.of(context).primaryColor,
          onTap: (index) {
            setState(() {
              _selectedIndex = index;
              switch (index) {
                case 0:
                  Navigator.pushReplacementNamed(context, 'home');
                  break;
                case 1:
                  break;
                case 2:
                  Navigator.pushReplacementNamed(context, 'album');
                  break;
                case 3:
                  Navigator.pushReplacementNamed(context, 'juegos');
                  break;
                default:
              }
            });
          },
        ),
        body: futB(context)
      ),
    );
  }

  Widget futB (BuildContext context) {
    return FutureBuilder(
      future: DBProvider.db.getAnimales(),
      builder:(BuildContext context, AsyncSnapshot<List> snapshot){
        if (snapshot.hasData) {
          return crearBody(snapshot.data);
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget crearBody(List <AnimalModel> animal){
    return Container(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
               _crearSwiper(animal),
              _crearGridView(animal)
              //_crearGrid(),
            ],
          ),
        );
  }

  Widget _crearSwiper(List<AnimalModel> animal) {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height,
      child: Swiper(
        itemBuilder: (BuildContext context, int index) {
          return new Image.network(
            animal[index].rutaImg,
            fit: BoxFit.cover,
          );
        },
        itemCount: animal.length-1,
        pagination: new SwiperPagination(),
        control: new SwiperControl(),
        autoplay: true,
        loop: true,
        onTap: (index) {
          Navigator.of(context).pushNamed('animalDetalles',arguments: animal[index]);
        },
      ),
    );
  }

  Widget _crearGridView(List <AnimalModel> animal) {
    return Container(
      width: double.infinity,
      child: StaggeredGridView.countBuilder(
        physics: ScrollPhysics(),
        shrinkWrap: true,
        crossAxisCount: 4,
        itemCount: animal.length-1,
        itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: (){
                 Navigator.of(context).pushNamed('animalDetalles',arguments: animal[index]);
              },
            child: new Image.network(
              animal[index].rutaImg,
              fit: BoxFit.cover,
          ),
            );
        },
        staggeredTileBuilder: (int index) =>
            new StaggeredTile.count(2, index.isEven ? 2 : 1),
        mainAxisSpacing: 4.0,
        crossAxisSpacing: 4.0,
      ),
    );
  }

  obtenerAnimales() async {
    List<AnimalModel> list = await DBProvider.db.getAnimales();
    list.forEach((animal) {
      //print(animal.nombre);
    });
  }

}
