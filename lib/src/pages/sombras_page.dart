import 'dart:math';

import 'package:animalapp/src/models/siluetas_model.dart';
import 'package:animalapp/src/providers/db_provider.dart';
import 'package:flutter/material.dart';

class SombrasPage extends StatefulWidget {
  SombrasPage({Key key}) : super(key: key);

  @override
  _SombrasPageState createState() => _SombrasPageState();
}

class _SombrasPageState extends State<SombrasPage> {

  int index = 1;
  bool chb = false;
  bool yaPaso = false;
  String _resp = "";
  int respCorrectas = 0;
  String respC = "";
  List<String> respuestas = new List();
  String rutaIMG = "";

  @override
  Widget build(BuildContext context) {

    //List<CuestionarioModel> preg = obtenerPreguntas();
    
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Theme.of(context).primaryColorDark,
        title: Text('Juego de la sombra',textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),),
      ),
      backgroundColor: Theme.of(context).primaryColorDark,
      body: ftrBuild(),
    );
  }

  Widget ftrBuild() {
    return FutureBuilder(
      future: DBProvider.db.getSiluets(),
      builder: (BuildContext context, AsyncSnapshot<List<SiluetasModel>> snapshot) {
        if(snapshot.hasData){
          return body(snapshot.data);
        }else{
          return Center(child: CircularProgressIndicator(),);
        }
      },
    );
  }

  Widget body (List<SiluetasModel> preg) {
    
    if (!yaPaso) {
      preg.shuffle();

      respC = preg[0].resC;
      rutaIMG = preg[0].rutaSilueta;

      respuestas.add(preg[0].resC);
      respuestas.add(preg[0].resiA);
      respuestas.add(preg[0].resiB);
      respuestas.add(preg[0].resiC);
      respuestas.add(preg[0].resiD);
      respuestas.shuffle();
      yaPaso = true;
    }

    return SingleChildScrollView(
        padding: EdgeInsets.all(16),
        child: Column(
          children: <Widget>[
            Text(
              '¡Responde qué animal es correctamente para ganar una estampa para tu album!',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
            Padding(padding: EdgeInsets.all(8)),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.network(rutaIMG, fit: BoxFit.cover,),
            ),
            Padding(padding: EdgeInsets.all(8)),
            RadioListTile(
              title: Text(
                respuestas[0],
                style: TextStyle(color: Colors.white),
              ),
              activeColor: Colors.white,
              value: respuestas[0],
              groupValue: _resp,
              onChanged: (value) {
                setState(() {
                  _resp = value;
                });
              },
            ),
            RadioListTile(
              title: Text(
                respuestas[1],
                style: TextStyle(color: Colors.white),
              ),
              activeColor: Colors.white,
              value: respuestas[1],
              groupValue: _resp,
              onChanged: (value) {
                setState(() {
                  _resp = value;
                });
              },
            ),
            RadioListTile(
              title: Text(
                respuestas[2],
                style: TextStyle(color: Colors.white),
              ),
              activeColor: Colors.white,
              value: respuestas[2],
              groupValue: _resp,
              onChanged: (value) {
                setState(() {
                  _resp = value;
                });
              },
            ),
            RadioListTile(
              title: Text(
                respuestas[3],
                style: TextStyle(color: Colors.white),
              ),
              activeColor: Colors.white,
              value: respuestas[3],
              groupValue: _resp,
              onChanged: (value) {
                setState(() {
                  _resp = value;
                });
              },
            ),
            RadioListTile(
              title: Text(
                respuestas[4],
                style: TextStyle(color: Colors.white),
              ),
              activeColor: Colors.white,
              value: respuestas[4],
              groupValue: _resp,
              onChanged: (value) {
                setState(() {
                  _resp = value;
                });
              },
            ),
            SizedBox(
              width: double.infinity,
              child: RaisedButton(
                child: Text('ENVIAR'),
                onPressed: () {
                  setState(() {
                    if (respC == _resp) {
                      Navigator.pushReplacementNamed(context, 'estampa');
                    }else{
                      Navigator.pushReplacementNamed(context, 'perdioCromo');
                    }
                  });
                },
              ),
            )
          ],
        ),
      );
  }

  obtenerPreguntas() async{
    List<SiluetasModel> lista = await DBProvider.db.getSiluets();
    Random rnd = new Random();
    int min = 1;
    int cant = 0;
    List<int> listId = new List();
    List<SiluetasModel> preguntas = new List();
    while (cant < 3) {
      int r = min + rnd.nextInt(lista.length);
      if (!listId.contains(r)) {
        listId.add(r);
        preguntas.add(lista.elementAt(r-1));
        cant++;
      }
    }
    return preguntas;
  }

}
