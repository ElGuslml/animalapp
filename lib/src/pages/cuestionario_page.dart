import 'dart:math';

import 'package:animalapp/src/models/cuestionario_model.dart';
import 'package:animalapp/src/providers/db_provider.dart';
import 'package:flutter/material.dart';

class CuestionarioPage extends StatefulWidget {
  CuestionarioPage({Key key}) : super(key: key);

  @override
  _CuestionarioPageState createState() => _CuestionarioPageState();
}

class _CuestionarioPageState extends State<CuestionarioPage> {

  int index = 1;
  bool chb = false;
  bool yaPaso = false;
  String _resp = "";
  int respCorrectas = 0;
  List<String> respC = new List();

  @override
  Widget build(BuildContext context) {

    //List<CuestionarioModel> preg = obtenerPreguntas();
    
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Theme.of(context).primaryColorDark,
        title: Text('Cuestionario'),
      ),
      backgroundColor: Theme.of(context).primaryColorDark,
      body: ftrBuild(),
    );
  }

  Widget ftrBuild() {
    return FutureBuilder(
      future: DBProvider.db.getCantCuest(),
      builder: (BuildContext context, AsyncSnapshot<List<CuestionarioModel>> snapshot) {
        if(snapshot.hasData){
          return body(snapshot.data);
        }else{
          return Center(child: CircularProgressIndicator(),);
        }
      },
    );
  }

  Widget body (List<CuestionarioModel> preg) {
    respC.add(preg[0].resC);
    respC.add(preg[1].resC);
    respC.add(preg[2].resC);

    List<String> res = new List();
    res.add(preg[index-1].resC);
    res.add(preg[index-1].resIA);
    res.add(preg[index-1].resIB);
    res.add(preg[index-1].resIC);
    res.add(preg[index-1].resID);
    if (!yaPaso) {
      res.shuffle();
      yaPaso = true;
    }

    return Container(
        padding: EdgeInsets.all(16),
        child: Column(
          children: <Widget>[
            Text(
              'Contesta las siguientes preguntas correctamente para ganar 3 estampas para tu album.',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
            Padding(padding: EdgeInsets.all(8)),
            Text(
              'Pregunta $index de 3',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
            Padding(padding: EdgeInsets.all(8)),
            Text(
              preg[index-1].pregunta,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
            Padding(padding: EdgeInsets.all(8)),
            RadioListTile(
              title: Text(
                res[0],
                style: TextStyle(color: Colors.white),
              ),
              activeColor: Colors.white,
              value: res[0],
              groupValue: _resp,
              onChanged: (value) {
                setState(() {
                  _resp = value;
                });
              },
            ),
            RadioListTile(
              title: Text(
                res[1],
                style: TextStyle(color: Colors.white),
              ),
              activeColor: Colors.white,
              value: res[1],
              groupValue: _resp,
              onChanged: (value) {
                setState(() {
                  _resp = value;
                });
              },
            ),
            RadioListTile(
              title: Text(
                res[2],
                style: TextStyle(color: Colors.white),
              ),
              activeColor: Colors.white,
              value: res[2],
              groupValue: _resp,
              onChanged: (value) {
                setState(() {
                  _resp = value;
                });
              },
            ),
            RadioListTile(
              title: Text(
                res[3],
                style: TextStyle(color: Colors.white),
              ),
              activeColor: Colors.white,
              value: res[3],
              groupValue: _resp,
              onChanged: (value) {
                setState(() {
                  _resp = value;
                });
              },
            ),
            RadioListTile(
              title: Text(
                res[4],
                style: TextStyle(color: Colors.white),
              ),
              activeColor: Colors.white,
              value: res[4],
              groupValue: _resp,
              onChanged: (value) {
                setState(() {
                  _resp = value;
                });
              },
            ),
            SizedBox(
              width: double.infinity,
              child: RaisedButton(
                child: _btnFinal(),
                onPressed: () {
                  setState(() {
                    if(index + 1 > 3){
                      if (_resp != "") {
                        if (_resp == respC[index - 1]) {
                          
                          respCorrectas++;
                          print(respCorrectas);
                        }
                      print("terminó");
                      if (respCorrectas >= 3) {
                        Navigator.pushReplacementNamed(context, 'ganoCromos');
                      }else{
                        Navigator.pushReplacementNamed(context, 'perdioCromo');
                      }
                      }
                    }else{
                      if (_resp != "") {
                        if (_resp == respC[index - 1]) {
                          
                          respCorrectas++;
                          print(respCorrectas);
                        }
                        index = index + 1;
                        yaPaso = false;
                        //print(index);
                        
                      }
                    }
                  });
                },
              ),
            )
          ],
        ),
      );
  }

  Widget _btnFinal(){
    if (index + 1 > 3) {
      return  Text('TERMINAR');
    }else{
      return  Text('SIGUIENTE');
    }
  }

  obtenerPreguntas() async{
    List<CuestionarioModel> lista = await DBProvider.db.getCantCuest();
    Random rnd = new Random();
    int min = 1;
    int cant = 0;
    List<int> listId = new List();
    List<CuestionarioModel> preguntas = new List();
    while (cant < 3) {
      int r = min + rnd.nextInt(lista.length);
      if (!listId.contains(r)) {
        listId.add(r);
        preguntas.add(lista.elementAt(r-1));
        cant++;
      }
    }
    return preguntas;
  }

}
