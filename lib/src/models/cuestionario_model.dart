import 'dart:convert';

CuestionarioModel cuestionarioModelFromJson(String str) => CuestionarioModel.fromJson(json.decode(str));

String cuestionarioModelToJson(CuestionarioModel data) => json.encode(data.toJson());

class CuestionarioModel {

  int idCuestionario;
  String pregunta;
  String resC;
  String resIA;
  String resIB;
  String resIC;
  String resID;
  String rutaImg;
  String rutaHab;

  CuestionarioModel({
    this.idCuestionario,
    this.pregunta,
    this.resC,
    this.resIA,
    this.resIB,
    this.resIC,
    this.resID,
  });

  factory CuestionarioModel.fromJson(Map<String, dynamic> json) => CuestionarioModel(
        idCuestionario: json["id_cuestionario"],
        pregunta: json["pregunta"],
        resC: json["res_c"],
        resIA: json["res_ia"],
        resIB: json["res_ib"],
        resIC: json["res_ic"],
        resID: json["res_id"],
    );

    Map<String, dynamic> toJson() => {
        "id_cuestionario": idCuestionario,
        "pregunta": pregunta,
        "res_c": resC,
        "res_ia": resIA,
        "res_ib": resIB,
        "res_ic": resIC,
        "res_id" : resID,
    };

}