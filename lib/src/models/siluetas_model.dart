import 'dart:convert';

SiluetasModel siluetasModelFromJson(String str) => SiluetasModel.fromJson(json.decode(str));

String siluetasModelToJson(SiluetasModel data) => json.encode(data.toJson());

class SiluetasModel {

  int idSilueta;
  String rutaSilueta;
  String resC;
  String resiA;
  String resiB;
  String resiC;
  String resiD;
  

  SiluetasModel({
    this.idSilueta,
    this.rutaSilueta,
    this.resC,
    this.resiA,
    this.resiB,
    this.resiC,
    this.resiD,
  });

  factory SiluetasModel.fromJson(Map<String, dynamic> json) => SiluetasModel(
        idSilueta: json["id"],
        rutaSilueta: json["silueta"],
        resC: json["res_c"],
        resiA: json["res_ia"],
        resiB: json["res_ib"],
        resiC: json["res_ic"],
        resiD: json["res_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": idSilueta,
        "silueta": rutaSilueta,
        "res_c": resC,
        "res_ia": resiA,
        "res_ib": resiB,
        "res_ic": resiC,
        "res_id" : resiD,
    };

}