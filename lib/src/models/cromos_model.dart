import 'dart:convert';

CromosModel cromosModelFromJson(String str) => CromosModel.fromJson(json.decode(str));

String cromosModelToJson(CromosModel data) => json.encode(data.toJson());

class CromosModel {

  int idCromo;
  String nombre;
  String status;
  String rutaImg;

  CromosModel({
    this.idCromo,
    this.nombre,
    this.status,
    this.rutaImg
  });

  factory CromosModel.fromJson(Map<String, dynamic> json) => CromosModel(
        idCromo: json["id_cromo"],
        nombre: json["nombre"],
        status: json["status"],
        rutaImg: json["ruta_img"],
    );

    Map<String, dynamic> toJson() => {
        "id_cromo": idCromo,
        "nombre": nombre,
        "status": status,
        "ruta_img": rutaImg,
    };

}