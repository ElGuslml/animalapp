import 'dart:convert';

AnimalModel animalModelFromJson(String str) => AnimalModel.fromJson(json.decode(str));

String animalModelToJson(AnimalModel data) => json.encode(data.toJson());

class AnimalModel {

  int idAnimal;
  String nombre;
  String status;
  String descripcion;
  String habitat;
  String lugar;
  String coordenadas;
  String rutaImg;
  String rutaHab;
  String rutaSonido;

  AnimalModel({
    this.idAnimal,
    this.nombre,
    this.status,
    this.descripcion,
    this.habitat,
    this.lugar,
    this.coordenadas,
    this.rutaImg,
    this.rutaHab,
    this.rutaSonido
  });

  factory AnimalModel.fromJson(Map<String, dynamic> json) => AnimalModel(
        idAnimal: json["id_animal"],
        nombre: json["nombre"],
        status: json["status"],
        descripcion: json["descripcion"],
        habitat: json["habitat"],
        lugar: json["lugar"],
        coordenadas: json["coord"],
        rutaImg: json["ruta_img"],
        rutaHab: json["ruta_habitat"],
        rutaSonido: json["ruta_sonido"],
    );

    Map<String, dynamic> toJson() => {
        "id_animal": idAnimal,
        "nombre": nombre,
        "status": status,
        "descripcion": descripcion,
        "habitat": habitat,
        "lugar": lugar,
        "coord" : coordenadas,
        "ruta_img" : rutaImg,
        "ruta_habitat" : rutaHab,
        "ruta_sonido" : rutaSonido
    };

}